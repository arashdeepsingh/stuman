/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLPreviewStudentController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Canvas canvas;
    @FXML
    Button editbtn;
    int stuid;
    ResultSet rs;
    String name, sid, ccertid, fname, mname, address, contact, email, dob, gender, nationality, religion, cast, mitualstatus, employmentstatus, qualifaction;
    Date admissiondate;
    public void setid(int id)throws IOException, JSONException{
        stuid = id;
        try {
            conn connection = new conn();
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();

            rs = stmt.executeQuery("SELECT * FROM `students` as `S` WHERE `S`.id =" + id);
            while (rs.next()) {
                name = rs.getString("name");
                sid = rs.getString("id");
                ccertid = rs.getString("ccert_id");
                fname = rs.getString("father_name");
                mname = rs.getString("mother_name");
                address = rs.getString("address");
                contact = rs.getString("contact");
                email = rs.getString("email");
                dob = rs.getString("dateofbirth");
                gender = rs.getString("gender");
                nationality = rs.getString("nationality");
                religion = rs.getString("religion");
                cast = rs.getString("cast");
                mitualstatus = rs.getString("mitualstatus");
                employmentstatus = rs.getString("employmentstatus");
                qualifaction = rs.getString("qualifaction");
                admissiondate = rs.getDate("admission_date");
            }

            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.rect(10, 10, 560, 410);
            gc.setStroke(javafx.scene.paint.Color.BLUE);
            gc.setLineWidth(1);
            gc.stroke();

            gc.setFont(Font.font("", FontWeight.BOLD, 20));
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("AIC Computer Education", 280, 30);
            gc.setFont(Font.font("", FontWeight.LIGHT, 12));
            gc.fillText("New Vishwakarma Nager, Focal Point, Ludhiana-10", 280, 45);
            gc.setTextAlign(TextAlignment.LEFT);
            gc.fillText("Student Name: ", 20, 110);
            gc.fillText("Father Name: ", 20, 130);
            gc.fillText("Mother Name: ", 20, 150);
            gc.fillText("CCERT ID: ", 20, 170);
            gc.fillText("Address: ", 20, 190);
            gc.fillText("Contact No: ", 20, 210);
            gc.fillText("Email No: ", 20, 230);

            gc.fillText("Gender: ", 20, 250);
            gc.fillText("Nationality: ", 20, 270);
            gc.fillText("Religion: ", 20, 290);
            gc.fillText("Cast: ", 20, 310);
            gc.fillText("Mirtual Status: ", 20, 330);
            gc.fillText("Employment Status: ", 20, 350);
            gc.fillText("Qualifaction: ", 20, 370);
            gc.fillText("Date Of Birth: ", 20, 390);
            gc.fillText("Admission Date: ", 20, 410);

            gc.setFont(Font.font("", FontWeight.BOLD, 16));
            gc.fillText(name, 150, 110);
            gc.fillText(fname, 150, 130);
            gc.fillText(mname, 150, 150);
            gc.fillText(ccertid, 150, 170);
            gc.fillText(address, 150, 190);
            gc.fillText(contact, 150, 210);
            gc.fillText(email, 150, 230);

            gc.fillText(gender, 150, 250);
            gc.fillText(nationality, 150, 270);
            gc.fillText(religion, 150, 290);
            gc.fillText(cast, 150, 310);
            gc.fillText(mitualstatus, 150, 330);
            gc.fillText(employmentstatus, 150, 350);
            gc.fillText(qualifaction, 150, 370);
            gc.fillText(dob, 150, 390);
            gc.fillText(admissiondate.toString(), 150, 410);

            editbtn.setOnAction((ActionEvent event) -> {
                try {
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLEditStudent.fxml"));
                    try {
                        Loader.load();
                    } catch (LoadException ex) {
                        System.out.println(ex);
                    }
                    
                    Parent logparent = Loader.getRoot();
                    Scene scene = new Scene(logparent);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    app_stage.setTitle("Admin Here!");
                    app_stage.setScene(scene);
                    app_stage.show();
                    FXMLEditStudentController toedit = Loader.getController();
                    int editid = Integer.parseInt(sid);
                    toedit.setid(editid);
                } catch (IOException ioe) {
                    
                } catch (SQLException | JSONException ex) {
                    Logger.getLogger(FXMLPreviewStudentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        } catch (SQLException sqle) {
        }
    }

    @FXML
    private void backAction(ActionEvent event) throws JSONException {
        try {
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLListStu.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
            FXMLListStuController tolist = Loader.getController();           
            tolist.setid();
        } catch (IOException ioe) {

        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

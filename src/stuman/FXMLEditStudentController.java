/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLEditStudentController implements Initializable {

    @FXML
    TextField namef, fnamef, ccertidf, mothernamef, contactf, emailf, nationf, religionf, castf, ediqualificationf;
    @FXML
    DatePicker dobf, addmissiondf;
    @FXML
    TextArea addressf;
    @FXML
    RadioButton gmale, gfemale, msm, msum, ese, esse, eue, ess;

    @FXML
    Label label, label2;
    int id;
    String ccertidstr;
    String name, sid, ccertid, fname, mname, address, contact, email, gender, nationality, religion, cast, mitualstatus, employmentstatus, qualifaction;
    Date dob, admissiondate;

    String genderch;
    String employstatus;
    String mitualstat;
    ResultSet rs;

    public void setid(int uid) throws SQLException, IOException,JSONException{
        gfemale.setSelected(false);
        id = uid;
        try {
            conn connection = new conn();
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();

            rs = stmt.executeQuery("SELECT * FROM `students` as `S` WHERE `S`.`id` =" + id);
            while (rs.next()) {
                name = rs.getString("name");
                sid = rs.getString("id");
                ccertid = rs.getString("ccert_id");
                fname = rs.getString("father_name");
                mname = rs.getString("mother_name");
                address = rs.getString("address");
                contact = rs.getString("contact");
                email = rs.getString("email");
                dob = rs.getDate("dateofbirth");
                gender = rs.getString("gender");
                nationality = rs.getString("nationality");
                religion = rs.getString("religion");
                cast = rs.getString("cast");
                mitualstatus = rs.getString("mitualstatus");
                employmentstatus = rs.getString("employmentstatus");
                qualifaction = rs.getString("qualifaction");
                admissiondate = rs.getDate("dateofbirth");

            }
            namef.setText(name);
            fnamef.setText(fname);
            ccertidf.setText(ccertid);
            mothernamef.setText(mname);
            addressf.setText(address);
            contactf.setText(contact);
            emailf.setText(email);
            nationf.setText(nationality);
            religionf.setText(religion);
            castf.setText(cast);
            ediqualificationf.setText(qualifaction);
            dobf.setValue(dob.toLocalDate());
            addmissiondf.setValue(admissiondate.toLocalDate());
            if ("Male".equals(gender)) {
                gmale.setSelected(true);
            } else {
                gfemale.setSelected(true);
            }
            if ("Married".equals(mitualstatus)) {
                msm.setSelected(true);
            } else {
                msum.setSelected(true);
            }
            if ("Employed".equals(employmentstatus)) {
                ese.setSelected(true);
            }
            if ("Unemployed".equals(employmentstatus)) {
                eue.setSelected(true);
            }
            if ("Self Employed".equals(employmentstatus)) {
                esse.setSelected(true);
            }
            if ("Student".equals(employmentstatus)) {
                ess.setSelected(true);
            }

            if (gmale.isSelected()) {
                genderch = "Male";
            } else {
                genderch = "Female";
            }

            if (msm.isSelected()) {
                mitualstat = "Married";
            }
            if (msum.isSelected()) {
                mitualstat = "Unmarried";
            }
            if (ese.isSelected()) {
                employstatus = "Employed";
            }
            if (esse.isSelected()) {
                employstatus = "Self Employed";
            }
            if (eue.isSelected()) {
                employstatus = "UnEmployed";
            }
            if (ess.isSelected()) {
                employstatus = "Student";
            }
        } catch (SQLException slqe) {

        }
    }

    @FXML
    private void submitAction(ActionEvent event) throws JSONException {
        if (gmale.isSelected()) {
            genderch = "Male";
        } else {
            genderch = "Female";
        }

        if (msm.isSelected()) {
            mitualstat = "Married";
        }
        if (msum.isSelected()) {
            mitualstat = "Unmarried";
        }
        if (ese.isSelected()) {
            employstatus = "Employed";
        }
        if (esse.isSelected()) {
            employstatus = "Self Employed";
        }
        if (eue.isSelected()) {
            employstatus = "UnEmployed";
        }
        if (ess.isSelected()) {
            employstatus = "Student";
        }
        try {
            name = namef.getText();
            fname = fnamef.getText();
            if (isInt(ccertidf)) {
                ccertidstr = ccertidf.getText();
                // ccertid = Integer.parseInt(ccertidstr);
            }
            conn connection = new conn();
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();
            if ("".equals(name) || "".equals(fname) || "".equals(ccertidf)) {
                label2.setText("Enter in Fields");
            } else {
                Double contactint = Double.parseDouble(contactf.getText());
                String sql = "UPDATE `students` SET `name`='" + namef.getText() + "',`ccert_id`='" + ccertidf.getText() + "',`father_name`='" + fnamef.getText() + "',`mother_name`='" + mothernamef.getText() + "',`address`='" + addressf.getText() + "',`contact`=" + contactint + ",`email`='" + emailf.getText() + "',`dateofbirth`='" + dobf.getValue().toString() + "',`gender`='" + genderch + "',`nationality`='" + nationf.getText() + "',`religion`='" + religionf.getText() + "',`cast`='" + castf.getText() + "',`mitualstatus`='" + mitualstat + "',`employmentstatus`='" + employstatus + "',`qualifaction`='" + ediqualificationf.getText() + "' WHERE id=" + id;
                stmt.executeUpdate(sql);
            }

            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLPreviewStudent.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
            FXMLPreviewStudentController topreview = Loader.getController();
            topreview.setid(id);
        } catch (IOException | SQLException ioe) {

        }

    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        try {
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLPreviewStudent.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
            FXMLPreviewStudentController topreview = Loader.getController();
            try {
                topreview.setid(id);
            } catch (JSONException ex) {
                Logger.getLogger(FXMLEditStudentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ioe) {

        }
    }

    private boolean isDouble(TextField input) {
        try {
            double num = Double.parseDouble(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    private boolean isInt(TextField input) {
        try {
            int num = Integer.parseInt(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

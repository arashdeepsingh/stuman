/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

/**
 *
 * @author arashdeep
 */
public class ReceptBin {

    int receptId;
    String stuid;
    String mode;
    int amt;
    String amt_for;
    String dated;

    public ReceptBin(int receptId, String stuid, String mode, int amt, String amt_for, String dated) {
        this.receptId = receptId;
        this.stuid = stuid;
        this.mode = mode;
        this.amt = amt;
        this.amt_for = amt_for;
        this.dated = dated;
    }

    public void setReceptId(int receptId) {
        this.receptId = receptId;
    }

    public void setStuid(String stuid) {
        this.stuid = stuid;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setAmt(int amt) {
        this.amt = amt;
    }

    public void setAmt_for(String amt_for) {
        this.amt_for = amt_for;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

    public int getReceptId() {
        return receptId;
    }

    public String getStuid() {
        return stuid;
    }

    public String getMode() {
        return mode;
    }

    public int getAmt() {
        return amt;
    }

    public String getAmt_for() {
        return amt_for;
    }

    public String getDated() {
        return dated;
    }

   

}

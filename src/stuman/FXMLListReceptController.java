/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLListReceptController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    TableView<ReceptBin> recept;
    @FXML
    TableColumn<ReceptBin, Integer> receptId;
    @FXML
    TableColumn<ReceptBin, String> stuid;
    @FXML
    TableColumn<ReceptBin, String> mode;
    @FXML
    TableColumn<ReceptBin, Integer> amt;
    @FXML
    TableColumn<ReceptBin, String> amt_for;
    @FXML
    TableColumn<ReceptBin, String> dated;
    @FXML
    Label selid;
    @FXML
    Label lbl;
    String strid;
    int sell;
    int iid;

    public void getid() throws IOException, JSONException {
        conn connection = new conn();
        try {
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM `recept`";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String studentId = rs.getString("student_id_name");
                int stuid = rs.getInt("id");
                String amt_mode = rs.getString("payment_mode");
                int amount = rs.getInt("amount");
                String amount_for = rs.getString("amount_for");
                String date = rs.getString("datedon");

                recept.getItems().add(new ReceptBin(stuid, studentId, amt_mode, amount, amount_for, date));
            }

        } catch (SQLException exp) {
            Logger.getLogger(FXMLListReceptController.class.getName()).log(Level.SEVERE, null, exp);
        }
        recept.setOnMouseClicked((MouseEvent event) -> {
            ReceptBin selectedid = recept.getSelectionModel().getSelectedItem();
            sell = selectedid.getReceptId();
            strid = Integer.toString(sell);
            selid.setText(strid);
        });

        receptId.setCellValueFactory(new PropertyValueFactory<>("receptId"));
        stuid.setCellValueFactory(new PropertyValueFactory<>("stuid"));
        mode.setCellValueFactory(new PropertyValueFactory<>("mode"));
        amt.setCellValueFactory(new PropertyValueFactory<>("amt"));
        amt_for.setCellValueFactory(new PropertyValueFactory<>("amt_for"));
        dated.setCellValueFactory(new PropertyValueFactory<>("dated"));
    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        Parent logparent = FXMLLoader.load(getClass().getResource("FXMLAdmin.fxml"));
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        logparent.setUserData("hello");
        app_stage.show();
    }

    @FXML
    private void viewAction(ActionEvent event) throws IOException, JSONException {

        if (sell != 0) {
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLReceptView.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
            FXMLReceptViewController toviewrecept = Loader.getController();
            toviewrecept.stuid(sell);
        } else {
            lbl.setText("Please Select One");
        }
    }
    String csv;
    Stage st = new Stage();
    ResultSet result;

    @FXML
    private void csvAction(ActionEvent event) throws SQLException, IOException {
        FileChooser filechooser = new FileChooser();
        filechooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CComma Delimited (*.csv)", "*.csv"));
        FileWriter file = new FileWriter(filechooser.showSaveDialog(st));

        BufferedWriter Writer = new BufferedWriter(file);
        Writer.write("Name,Payment Mode,Amount,Amount For, Dated On\n");

        conn connection = new conn();
        Connection conn = null;
        try {
            conn = connection.conn();
        } catch (JSONException ex) {
            Logger.getLogger(FXMLListStuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM `recept`";
        // System.out.println(id);
        //   System.out.println(sql);
        result = stmt.executeQuery(sql);
        while (result.next()) {
            String name = result.getString("student_id_name");
            String payment_mode = result.getString("payment_mode");
            String amount = result.getString("amount");
            String amount_for = result.getString("amount_for");
            String datedon = result.getString("datedon");

            csv = name + ", " + payment_mode + ", " + amount + ", " + amount_for + ", \"" + datedon + "\"\n";
            Writer.write(csv);

        }
        Writer.flush();
        Writer.close();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}

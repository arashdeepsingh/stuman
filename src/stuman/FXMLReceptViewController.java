/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.control.Label;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLReceptViewController implements Initializable {

    @FXML
    Label jobStatus;
    @FXML
    Canvas can;
    String rid, name, mode, amtfor, amt, date;

    int iid;

    public void stuid(int id)throws IOException, JSONException{

        GraphicsContext gc = can.getGraphicsContext2D();
        gc.rect(10, 10, 440, 260);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("Recept", 220, 30);
        gc.fillText("New Vishwakarma Nager, Focal Point, Ludhiana-10", 220, 65);

        gc.setStroke(javafx.scene.paint.Color.BLUE);
        gc.setLineWidth(1);
        gc.stroke();
        try {
            conn connection = new conn();
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM `recept` WHERE id = " + id;
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                rid = rs.getString("id");
                System.out.println(rid);
                name = rs.getString("student_id_name");
                mode = rs.getString("payment_mode");
                amtfor = rs.getString("amount_for");
                amt = rs.getString("amount");
                date = rs.getString("datedon");

            }
            gc.setTextAlign(TextAlignment.LEFT);
            gc.fillText("Recept No.: " + rid, 20, 90);
            gc.fillText("Dated: " + date, 300, 90);

            gc.fillText("RECEIVED From: ", 20, 110);
            gc.fillText("Payment Mode: ", 20, 130);
            gc.fillText("Payment For: ", 20, 150);
            gc.fillText("Amount Rs.: ", 20, 170);

            gc.setFont(Font.font("", FontWeight.BOLD, 16));
            gc.fillText(name, 150, 110);
            gc.fillText(mode, 150, 130);
            gc.fillText(amtfor, 150, 150);
            gc.fillText(amt, 150, 170);

            // for stamp
            gc.setFont(Font.font("", FontWeight.LIGHT, 12));
            gc.setTextAlign(TextAlignment.RIGHT);

            gc.fillText("For AIC COMPUTER EDUCATION", 420, 200);
            gc.setFont(Font.font(8));
            gc.fillText("Manager/Auth. Sig./Incharge", 420, 240);

            // Center Name
            gc.setFont(Font.font("", FontWeight.BOLD, 20));
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("AIC Computer Education", 220, 50);
            // Center Name End
        } catch (SQLException sqle) {
        }

    }

    /**
     * Initializes the controller class.
     */
    @FXML
    private void printAction(ActionEvent event) {
        print(can);
    }

    @FXML
    private void backtostuAction(ActionEvent event) throws IOException, JSONException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLListStu.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
            System.out.println(ex);
        }

        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();
        FXMLListStuController tolist = Loader.getController();
        tolist.setid();
    }

    @FXML
    private void backAction(ActionEvent event) throws IOException, JSONException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLListRecept.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
            System.out.println(ex);
        }

        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();
        FXMLListReceptController torecept = Loader.getController();
        torecept.getid();
    }

    public void print(Node node) {
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT);

        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(node.getScene().getWindow())) {
            boolean success = job.printPage(node);
            if (success) {
                job.endJob();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}

-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 28, 2017 at 02:41 PM
-- Server version: 10.0.31-MariaDB-0ubuntu0.16.04.2
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stuman`
--

-- --------------------------------------------------------

--
-- Table structure for table `recept`
--

CREATE TABLE `recept` (
  `id` int(11) NOT NULL,
  `student_id_name` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `amount_for` varchar(225) NOT NULL,
  `datedon` date NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recept`
--

INSERT INTO `recept` (`id`, `student_id_name`, `payment_mode`, `amount`, `amount_for`, `datedon`, `userid`) VALUES
(15, '5', 'Cheque', 123, 'Fee', '2017-09-30', 1),
(16, '4', 'Cash', 10000, 'Fee', '2017-09-24', 1),
(17, '10', 'Cash', 12312312, 'Fee', '2017-09-24', 1),
(18, '20', 'Cheque', 10000, 'Fee', '2017-09-25', 1),
(19, '5', 'Cheque', 10000, 'Fee', '2017-09-22', 1),
(20, 'arashdeep singh[23]', 'Cheque', 123123, 'Fee', '2017-09-14', 1),
(21, 'arashdeep singh[22]', 'Cheque', 12323, 'Fee', '2017-09-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `ccert_id` int(11) NOT NULL,
  `father_name` varchar(60) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dateofbirth` varchar(25) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `religion` varchar(30) NOT NULL,
  `cast` varchar(50) NOT NULL,
  `mitualstatus` varchar(30) NOT NULL,
  `employmentstatus` varchar(60) NOT NULL,
  `qualifaction` varchar(255) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `ccert_id`, `father_name`, `mother_name`, `address`, `contact`, `email`, `dateofbirth`, `gender`, `nationality`, `religion`, `cast`, `mitualstatus`, `employmentstatus`, `qualifaction`, `userid`) VALUES
(21, 'rr', 1231222, 'rr', 'rr', 'asdasd', 1111111111, 'asdasd', '2017-08-30', 'Male', 'rr', 'sdas', 'dasds', 'Unmarried', 'Student', 'asdasd', 1),
(22, 'arashdeep singh', 1010212, 'gurjeet singh', 'kamaljeet kaur', '#7107,new vishwakarma nagar,focalpoing, ludhiana', 947889029, 'arash@delhiit.com', '2004-02-27', 'Male', 'arashdeep singh', 'sikh', 'singh', 'Unmarried', 'Student', '8th class', 1),
(23, 'arashdeep singh', 112125, 'gurjeet singh', 'kamaljeet kaur', '#7107,new vishwakarma nagar,focalpoint, ludhiana', 947889029, 'arash@delhiit.com', '2004-02-27', 'Male', 'arashdeep singh', 'sikh', 'singh', 'Unmarried', 'Student', '8th class', 1),
(24, 'asdasd', 12121, 'asdasdas', 'asdasd', 'asdasda', 111111111, 'asd', '2017-09-30', 'Male', 'asdasd', 'asd', 'asd', 'Unmarried', 'Student', 'asd', 1),
(25, 'ttest', 121362, 'ttes', 'ttest', 'arash', 11111111, 'asd', '2017-09-22', 'Male', 'ttest', 'asda', 'da', 'Unmarried', 'Student', 'asdasd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `type`) VALUES
(1, 'admin', '000111', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `recept`
--
ALTER TABLE `recept`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `recept`
--
ALTER TABLE `recept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `recept` (`userid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

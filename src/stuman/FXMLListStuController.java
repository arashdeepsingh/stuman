/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLListStuController implements Initializable {

    @FXML
    TableView<StuBin> student;
    @FXML
    TableColumn<StuBin, Integer> stuidt;
    @FXML
    TableColumn<StuBin, String> stunamet, stufnamet, mnamet, addresst, contactt, emailt, dobt, gendert, nationalityt, religiont, castt, mitualt, Employt, qualificationt, admissiondatet;
    @FXML
    TableColumn<StuBin, Integer> stuccertidt;
    @FXML
    TextField sname;
    @FXML
    Label lblerr;
    @FXML
    Button previewbtn;
    ResultSet rs;
    int selectedStudent;
    int iid;

    public void setid()throws IOException{
        try {
            conn connection = new conn();
            Connection conn = null;
            try {
                conn = connection.conn();
            } catch (JSONException ex) {
                Logger.getLogger(FXMLListStuController.class.getName()).log(Level.SEVERE, null, ex);
            }
            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM `students`";
            // System.out.println(id);
            //   System.out.println(sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String name = rs.getString("name");
                int stuid = rs.getInt("id");
                int ccertid = rs.getInt("ccert_id");
                String fname = rs.getString("father_name");
                String mname = rs.getString("mother_name");
                String address = rs.getString("address");
                String contact = rs.getString("contact");
                String email = rs.getString("email");
                String dob = rs.getString("dateofbirth");
                String gender = rs.getString("gender");
                String nationality = rs.getString("nationality");
                String religion = rs.getString("religion");
                String cast = rs.getString("cast");
                String mitualstatus = rs.getString("mitualstatus");
                String employmentstatus = rs.getString("employmentstatus");
                String qualifaction = rs.getString("qualifaction");
                String admissiondate = rs.getDate("admission_date").toString();

                student.getItems().add(new StuBin(stuid, ccertid, name, fname, mname, address, contact, email, dob, gender, nationality, religion, cast, mitualstatus, employmentstatus, qualifaction, admissiondate));

            }
        } catch (SQLException e) {
        }

        student.setEditable(true);
        student.setOnMouseClicked((MouseEvent event) -> {
            StuBin selectedid = student.getSelectionModel().getSelectedItem();
            selectedStudent = selectedid.getStuidt();
        });
        previewbtn.setOnAction((ActionEvent event) -> {
            if (selectedStudent != 0) {
                try {
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLPreviewStudent.fxml"));
                    try {
                        Loader.load();
                    } catch (LoadException ex) {
                        System.out.println(ex);
                    }
                    
                    Parent logparent = Loader.getRoot();
                    Scene scene = new Scene(logparent);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    app_stage.setTitle("Admin Here!");
                    app_stage.setScene(scene);
                    app_stage.show();
                    FXMLPreviewStudentController topreview = Loader.getController();
                    try {
                        topreview.setid(selectedStudent);
                    } catch (JSONException ex) {
                        
                    }
                } catch (IOException ioe) {
                    
                }
            } else {
                lblerr.setText("Please Select Student");
            }
        });
        stuidt.setCellValueFactory(new PropertyValueFactory<>("stuidt"));
        stunamet.setCellValueFactory(new PropertyValueFactory<>("stunamet"));
        stufnamet.setCellValueFactory(new PropertyValueFactory<>("stufnamet"));
        mnamet.setCellValueFactory(new PropertyValueFactory<>("mnamet"));
        stuccertidt.setCellValueFactory(new PropertyValueFactory<>("stuccertidt"));
        addresst.setCellValueFactory(new PropertyValueFactory<>("addresst"));
        contactt.setCellValueFactory(new PropertyValueFactory<>("contactt"));
        emailt.setCellValueFactory(new PropertyValueFactory<>("emailt"));
        dobt.setCellValueFactory(new PropertyValueFactory<>("dobt"));
        gendert.setCellValueFactory(new PropertyValueFactory<>("gendert"));
        nationalityt.setCellValueFactory(new PropertyValueFactory<>("nationalityt"));
        religiont.setCellValueFactory(new PropertyValueFactory<>("religiont"));
        castt.setCellValueFactory(new PropertyValueFactory<>("castt"));
        mitualt.setCellValueFactory(new PropertyValueFactory<>("mitualt"));
        Employt.setCellValueFactory(new PropertyValueFactory<>("Employt"));
        qualificationt.setCellValueFactory(new PropertyValueFactory<>("qualificationt"));
        admissiondatet.setCellValueFactory(new PropertyValueFactory<>("admissiondatet"));

    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        try {
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLAdmin.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
        } catch (IOException ioe) {
        }
    }

    @FXML
    private void receptAction(ActionEvent event) throws SQLException {
        if (selectedStudent != 0) {
            try {
                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLRecept.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                    System.out.println(ex);
                }

                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Admin Here!");
                app_stage.setScene(scene);
                app_stage.show();
                FXMLReceptController torecept = Loader.getController();
                try {
                    torecept.setid(selectedStudent);
                } catch (JSONException ex) {
                    Logger.getLogger(FXMLListStuController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ioe) {

            }
        } else {
            lblerr.setText("Please Select Student");
        }
    }

    public void print(Node node) {
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT);

        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(node.getScene().getWindow())) {
            boolean success = job.printPage(node);
            if (success) {
                job.endJob();
            }
        }
    }
    String csv;
    Stage st = new Stage();

    @FXML
    private void csvAction(ActionEvent event) throws SQLException, IOException {
        FileChooser filechooser = new FileChooser();
        filechooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CComma Delimited (*.csv)", "*.csv"));
        FileWriter file = new FileWriter(filechooser.showSaveDialog(st));

        BufferedWriter Writer = new BufferedWriter(file);
        Writer.write("Name,id,ccert id,father name,mother name, address,contact,email,date of birth,gander,nationality,religion,cast,mitual status,employment status, qualification, admission date\n");

        conn connection = new conn();
        Connection conn = null;
        try {
            conn = connection.conn();
        } catch (JSONException ex) {
            Logger.getLogger(FXMLListStuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM `students`";
        // System.out.println(id);
        //   System.out.println(sql);
        rs = stmt.executeQuery(sql);
        while (rs.next()) {
            String name = rs.getString("name");
            int stuid = rs.getInt("id");
            int ccertid = rs.getInt("ccert_id");
            String fname = rs.getString("father_name");
            String mname = rs.getString("mother_name");
            String address = rs.getString("address");
            String contact = rs.getString("contact");
            String email = rs.getString("email");
            String dob = rs.getString("dateofbirth");
            String gender = rs.getString("gender");
            String nationality = rs.getString("nationality");
            String religion = rs.getString("religion");
            String cast = rs.getString("cast");
            String mitualstatus = rs.getString("mitualstatus");
            String employmentstatus = rs.getString("employmentstatus");
            String qualifaction = rs.getString("qualifaction");
            String admissiondate = rs.getDate("admission_date").toString();
            csv = name + ", " + stuid + ", " + ccertid + ", " + fname + ", " + mname + ", \"" + address + "\", " + contact + ", " + email + ", " + dob + ", " + gender + ", " + nationality + ", " + religion + ", " + cast + ", " + mitualstatus + ", " + employmentstatus + ", " + qualifaction + ", " + admissiondate + "\n";
            Writer.write(csv);

        }
        Writer.flush();
        Writer.close();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 *
 * @author arashdeepsingh
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    TextField userf;
    @FXML
    PasswordField passf;
    @FXML
    Button button;
    @FXML
    Label lbl;

    String user;
    String pass;
    String type;
    int id;

    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException,JSONException{

        String username = userf.getText();
        String password = passf.getText();
        try {
            conn connection = new conn();
            Connection conn = connection.conn();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM user WHERE name='" + username + "' AND password='" + password + "'");
            while (rs.next()) {
                id = rs.getInt("id");
                user = rs.getString("name");
                pass = rs.getString("password");
                type = rs.getString("type");
            }

            if (user != null && pass != null) {

                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLAdmin.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                    System.out.println(ex);
                }

                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Admin Here!");
                app_stage.setScene(scene);
                app_stage.show();
                FXMLAdminController togetid = Loader.getController();
                togetid.getid(id);

            } else {
                lbl.setText("Username Or Password is Wrong");
            }

        } catch (SQLException | IOException e) {
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }

}

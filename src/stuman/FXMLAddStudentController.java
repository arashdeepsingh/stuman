/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.net.URL;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeepsingh
 */
public class FXMLAddStudentController implements Initializable {

    @FXML
    TextField namef, fnamef, ccertidf, mothernamef, contactf, emailf, nationf, religionf, castf, ediqualificationf;
    @FXML
    DatePicker dobf, addmissiondf;
    @FXML
    TextArea addressf;
    @FXML
    RadioButton gmale, gfemale, msm, msum, ese, esse, eue, ess;
    @FXML
    Label label, label2, weblbl;

    int ccertid;
    String name, fname, mname, address, contact, email, gender, nation, religion, cast, mitualstatus, employstatus, qualification;
    LocalDate dob, admissiondate;
  
    int iid;

    public void getid(int id) {
        iid = id;
    }

    public void toedit(String name, String fname, int ccertid, String mothername, String address, String contact, String email, LocalDate dobe, String gender, String nationality, String religion, String cast, String maritualstatus, String employmentstatus, String qualifaction,LocalDate admissiondate) {
        namef.setText(name);
        fnamef.setText(fname);
        String ccertstr = Integer.toString(ccertid);
        ccertidf.setText(ccertstr);
        mothernamef.setText(mothername);
        addressf.setText(address);
        contactf.setText(contact);
        emailf.setText(email);
        nationf.setText(nationality);
        religionf.setText(religion);
        castf.setText(cast);
        ediqualificationf.setText(qualifaction);
        dobf.setValue(dobe);
        addmissiondf.setValue((admissiondate));
        if ("Male".equals(gender)) {
            gmale.setSelected(true);
        } else {
            gfemale.setSelected(true);
        }
        if ("Married".equals(maritualstatus)) {
            msm.setSelected(true);
        } else {
            msum.setSelected(true);
        }
        if ("Employed".equals(employmentstatus)) {
            ese.setSelected(true);
        }
        if ("Unemployed".equals(employmentstatus)) {
            eue.setSelected(true);
        }
        if ("Self Employed".equals(employmentstatus)) {
            esse.setSelected(true);
        }
        if ("Student".equals(employmentstatus)) {
            ess.setSelected(true);
        }

    }

    @FXML
    private void submitAction(ActionEvent event) throws IOException, SQLException {
        name = namef.getText();
        fname = fnamef.getText();
        mname = mothernamef.getText();
        address = addressf.getText();
        contact = contactf.getText();
        email = emailf.getText();
        dob = dobf.getValue();
        admissiondate = addmissiondf.getValue();
        if (gmale.isSelected()) {
            gender = "Male";
        }
        if (gfemale.isSelected()) {
            gender = "Female";
        }
        nation = namef.getText();
        religion = religionf.getText();
        cast = castf.getText();
        if (msm.isSelected()) {
            mitualstatus = "Married";
        }
        if (msum.isSelected()) {
            mitualstatus = "Unmarried";
        }
        if (ese.isSelected()) {
            employstatus = "Employed";
        }
        if (esse.isSelected()) {
            employstatus = "Self Employed";
        }
        if (eue.isSelected()) {
            employstatus = "UnEmployed";
        }
        if (ess.isSelected()) {
            employstatus = "Student";
        }
        qualification = ediqualificationf.getText();

        if (isInt(ccertidf)) {
            String ccertidstr = ccertidf.getText();
            ccertid = Integer.parseInt(ccertidstr);
        }
        if (isDouble(contactf)) {
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLViewAdd.fxml"));
            try {
                Loader.load();
            } catch (LoadException ex) {
                System.out.println(ex);
            }

            Parent logparent = Loader.getRoot();
            Scene scene = new Scene(logparent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.setTitle("Admin Here!");
            app_stage.setScene(scene);
            app_stage.show();
            FXMLViewAddController toviewadd = Loader.getController();
            try {
                toviewadd.setStudent(name, fname, ccertid, mname, address, contact, email, dob, gender, nation, religion, cast, mitualstatus, employstatus, qualification,admissiondate);
            } catch (JSONException ex) {
                Logger.getLogger(FXMLAddStudentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            toviewadd.getid(iid);
        } else {
            label2.setText("Enter number in contact");
        }
    }

    @FXML
    private void BackAdminAction(ActionEvent event) throws IOException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLAdmin.fxml"));
        try{
        Loader.load();
        }catch(LoadException le){
        
        }
        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin");
        app_stage.setScene(scene);
        app_stage.show();
    }

    private boolean isDouble(TextField input) {
        try {
            Double num = Double.parseDouble(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
     private boolean isInt(TextField input) {
        try {
            int num = Integer.parseInt(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

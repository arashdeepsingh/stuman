/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLReceptController implements Initializable {

    @FXML
    ComboBox<String> mode_pay, amt_for;
    @FXML
    TextField amt_field;
    @FXML
    Button make;
    @FXML
    Label label;
    @FXML
    Label label2, receptfor;
    @FXML
    DatePicker dtpicker;
    String name;
    int stuid;
    ResultSet rssid;
    Integer amt;
    String amtfor;
    String amttype;
    Connection conn;
    String date;
    int genkey;

    public void setid(int id) throws SQLException, IOException, JSONException {
        conn connection = new conn();
        Connection conn = connection.conn();
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM `students` WHERE id =" + id;
        ResultSet rs = stmt.executeQuery(sql);
        ArrayList str = new ArrayList();
        while (rs.next()) {
            name = rs.getString("name");
        }
        receptfor.setText(name);
        mode_pay.getItems().addAll("Cash", "Cheque");
        mode_pay.setValue("NONE");
        amt_for.getItems().addAll("Fee");
        amt_for.setValue("NONE");
        make.setOnAction((ActionEvent event) -> {
            try {
                date = dtpicker.getValue().toString();
                if (isInt(amt_field)) {
                    if (mode_pay.getValue().equals("NONE")) {
                        label.setText("Select Mode To Pay");
                    } else {
                        if (amt_for.getValue().equals("NONE")) {
                            label.setText("Select Amount For");
                        } else {
                            amt = Integer.parseInt(amt_field.getText());
                            amtfor = amt_for.getValue();
                            amttype = mode_pay.getValue();
                            PreparedStatement sql1 = conn.prepareStatement("INSERT INTO `recept`(`student_id_name`, `payment_mode`, `amount`, `amount_for`, `datedon`) VALUES ('" + name + "','" + amttype + "'," + amt + ",'" + amtfor + "','" + date + "')", stmt.RETURN_GENERATED_KEYS);
                            sql1.executeUpdate();
                            ResultSet rs1 = sql1.getGeneratedKeys();
                            while (rs1.next()) {
                                genkey = rs1.getInt(1);
                            }
                            System.out.println(genkey);

                            label2.setText("Recept Entered");
                            mode_pay.setValue("NONE");
                            amt_for.setValue("NONE");
                            amt_field.setText(null);
                            try {
                                FXMLLoader Loader = new FXMLLoader();
                                Loader.setLocation(getClass().getResource("FXMLReceptView.fxml"));
                                try {
                                    Loader.load();
                                } catch (LoadException ex) {
                                    System.out.println(ex);
                                }

                                Parent logparent = Loader.getRoot();
                                Scene scene = new Scene(logparent);
                                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                app_stage.setTitle("Admin Here!");
                                app_stage.setScene(scene);
                                app_stage.show();
                                FXMLReceptViewController toviewrecept = Loader.getController();
                                try {
                                    toviewrecept.stuid(genkey);
                                } catch (JSONException ex) {
                                }
                            } catch (IOException ioe) {

                            }
                        }
                    }
                } else {
                    label.setText("Enter Number In Amount");
                }
            } catch (SQLException sqle) {

            }
        });

    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        Parent logparent = FXMLLoader.load(getClass().getResource("FXMLAdmin.fxml"));
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    private boolean isInt(TextField input) {
        try {
            int num = Integer.parseInt(input.getText());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

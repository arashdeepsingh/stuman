/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeep
 */
public class FXMLViewAddController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Canvas canvas;
    @FXML
    Button editbtn, submitbtn;
    @FXML
    Label lblerr;
    int iid;

    public void getid(int id) {
        iid = id;

    }

    public void setStudent(String name, String fname, int ccertid, String mothername, String address, String contact, String email, LocalDate dob, String gender, String nationality, String religion, String cast, String maritualstatus, String employmentstatus, String qualifaction, LocalDate admissiondate) throws JSONException{
        String ccertidstr = Integer.toString(ccertid);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.rect(10, 10, 560, 400);
        gc.setStroke(javafx.scene.paint.Color.BLUE);
        gc.setLineWidth(1);
        gc.stroke();

        gc.setFont(Font.font("", FontWeight.BOLD, 20));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("AIC Computer Education", 280, 30);
        gc.setFont(Font.font("", FontWeight.LIGHT, 12));
        gc.fillText("New Vishwakarma Nager, Focal Point, Ludhiana-10", 280, 45);

        gc.setTextAlign(TextAlignment.LEFT);
        gc.fillText("Student Name: ", 20, 110);
        gc.fillText("Father Name: ", 20, 130);
        gc.fillText("Mother Name: ", 20, 150);
        gc.fillText("CCERT ID: ", 20, 170);
        gc.fillText("Address: ", 20, 190);
        gc.fillText("Contact No: ", 20, 210);
        gc.fillText("Email No: ", 20, 230);

        gc.fillText("Gender: ", 20, 250);
        gc.fillText("Nationality: ", 20, 270);
        gc.fillText("Religion: ", 20, 290);
        gc.fillText("Cast: ", 20, 310);
        gc.fillText("Mirtual Status: ", 20, 330);
        gc.fillText("Employment Status: ", 20, 350);
        gc.fillText("Qualifaction: ", 20, 370);
        gc.fillText("Date Of Birth: ", 20, 390);
        gc.fillText("Date Of Birth: ", 20, 390);
        gc.fillText("Admission Date: ", 20, 410);

        gc.setFont(Font.font("", FontWeight.BOLD, 16));
        gc.fillText(name, 150, 110);
        gc.fillText(fname, 150, 130);
        gc.fillText(mothername, 150, 150);
        gc.fillText(ccertidstr, 150, 170);
        gc.fillText(address, 150, 190);
        gc.fillText(contact, 150, 210);
        gc.fillText(email, 150, 230);
        gc.fillText(gender, 150, 250);
        gc.fillText(nationality, 150, 270);
        gc.fillText(religion, 150, 290);
        gc.fillText(cast, 150, 310);
        gc.fillText(maritualstatus, 150, 330);
        gc.fillText(employmentstatus, 150, 350);
        gc.fillText(qualifaction, 150, 370);
        gc.fillText(dob.toString(), 150, 390);
        gc.fillText(admissiondate.toString(), 150, 410);

        submitbtn.setOnAction((ActionEvent event) -> {
            try {
                conn connection = new conn();
                Connection conn = connection.conn();
                Statement stmt = conn.createStatement();
                if (ccertidcheck(ccertid) == true) {
                    String sql = "INSERT INTO `students`(`name`, `ccert_id`, `father_name`,`mother_name`,`address`,`contact`,`email`,`dateofbirth`,`gender`,`nationality`,`religion`,`cast`,`mitualstatus`,`employmentstatus`,`qualifaction`,`admission_date`) VALUES ('" + name + "', '" + ccertidstr + "', '" + fname + "', '" + mothername + "', '" + address + "', " + contact + ", '" + email + "',  '" + dob + "', '" + gender + "', '" + nationality + "', '" + religion + "', '" + cast + "', '" + maritualstatus + "', '" + employmentstatus + "', '" + qualifaction + "','" + admissiondate + "')";
                    stmt.execute(sql);
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(FXMLViewAddController.this.getClass().getResource("FXMLListStu.fxml"));
                    try {
                        Loader.load();
                    } catch (LoadException ex) {
                    }   Parent logparent = Loader.getRoot();
                    Scene scene = new Scene(logparent);
                    Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    app_stage.setTitle("Admin Here!");
                    app_stage.setScene(scene);
                    app_stage.show();
                    FXMLListStuController tolist = Loader.getController();
                    tolist.setid();
                } else {
                    lblerr.setText("CCERT Id Match");
                }
            }catch (SQLException | IOException | JSONException sqle) {
            }
        });

        editbtn.setOnAction((ActionEvent event) -> {
            try {
                FXMLLoader Loader = new FXMLLoader();
                Loader.setLocation(getClass().getResource("FXMLAddStudent.fxml"));
                try {
                    Loader.load();
                } catch (LoadException ex) {
                    System.out.println(ex);
                }

                Parent logparent = Loader.getRoot();
                Scene scene = new Scene(logparent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.setTitle("Admin Here!");
                app_stage.setScene(scene);
                app_stage.show();
                FXMLAddStudentController toedit = Loader.getController();
                toedit.toedit(name, fname, ccertid, mothername, address, contact, email, dob, gender, nationality, religion, cast, maritualstatus, employmentstatus, qualifaction, admissiondate);

            } catch (IOException ioe) {
            }
        });
    }

    public boolean ccertidcheck(int id) throws SQLException, IOException,JSONException{
        int chid = 0;
        conn connection = new conn();
        Connection conn = connection.conn();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM `students` WHERE ccert_id =" + id);
        while (rs.next()) {
            chid = rs.getInt("ccert_id");
        }

        if (chid == id) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

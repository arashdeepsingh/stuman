/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

/**
 *
 * @author arashdeep
 */
public class StuBin {

    int stuidt;
    int stuccertidt;
    String stunamet, stufnamet, mnamet, addresst, contactt, emailt, dobt, gendert, nationalityt, religiont, castt, mitualt, Employt, qualificationt,admissiondatet;

    public StuBin(int stuidt, int stuccertidt, String stunamet, String stufnamet, String mnamet, String addresst, String contactt, String emailt, String dobt, String gendert, String nationalityt, String religiont, String castt, String mitualt, String Employt, String qualificationt, String admissiondatet) {
        this.stuidt = stuidt;
        this.stuccertidt = stuccertidt;
        this.stunamet = stunamet;
        this.stufnamet = stufnamet;
        this.mnamet = mnamet;
        this.addresst = addresst;
        this.contactt = contactt;
        this.emailt = emailt;
        this.dobt = dobt;
        this.gendert = gendert;
        this.nationalityt = nationalityt;
        this.religiont = religiont;
        this.castt = castt;
        this.mitualt = mitualt;
        this.Employt = Employt;
        this.qualificationt = qualificationt;
        this.admissiondatet = admissiondatet;
    }

    public int getStuidt() {
        return stuidt;
    }

    public void setStuidt(int stuidt) {
        this.stuidt = stuidt;
    }

    public int getStuccertidt() {
        return stuccertidt;
    }

    public void setStuccertidt(int stuccertidt) {
        this.stuccertidt = stuccertidt;
    }

    public String getStunamet() {
        return stunamet;
    }

    public void setStunamet(String stunamet) {
        this.stunamet = stunamet;
    }

    public String getStufnamet() {
        return stufnamet;
    }

    public void setStufnamet(String stufnamet) {
        this.stufnamet = stufnamet;
    }

    public String getMnamet() {
        return mnamet;
    }

    public void setMnamet(String mnamet) {
        this.mnamet = mnamet;
    }

    public String getAddresst() {
        return addresst;
    }

    public void setAddresst(String addresst) {
        this.addresst = addresst;
    }

    public String getContactt() {
        return contactt;
    }

    public void setContactt(String contactt) {
        this.contactt = contactt;
    }

    public String getEmailt() {
        return emailt;
    }

    public void setEmailt(String emailt) {
        this.emailt = emailt;
    }

    public String getDobt() {
        return dobt;
    }

    public void setDobt(String dobt) {
        this.dobt = dobt;
    }

    public String getGendert() {
        return gendert;
    }

    public void setGendert(String gendert) {
        this.gendert = gendert;
    }

    public String getNationalityt() {
        return nationalityt;
    }

    public void setNationalityt(String nationalityt) {
        this.nationalityt = nationalityt;
    }

    public String getReligiont() {
        return religiont;
    }

    public void setReligiont(String religiont) {
        this.religiont = religiont;
    }

    public String getCastt() {
        return castt;
    }

    public void setCastt(String castt) {
        this.castt = castt;
    }

    public String getMitualt() {
        return mitualt;
    }

    public void setMitualt(String mitualt) {
        this.mitualt = mitualt;
    }

    public String getEmployt() {
        return Employt;
    }

    public void setEmployt(String Employt) {
        this.Employt = Employt;
    }

    public String getQualificationt() {
        return qualificationt;
    }

    public void setQualificationt(String qualificationt) {
        this.qualificationt = qualificationt;
    }

    public String getAdmissiondatet() {
        return admissiondatet;
    }

    public void setAdmissiondatet(String admissiondatet) {
        this.admissiondatet = admissiondatet;
    }

   

}

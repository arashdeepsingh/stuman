/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stuman;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author arashdeepsingh
 */
public class FXMLAdminController implements Initializable {

    int iid;

    public void getid(int id) {
        iid = id;
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLAddStudent.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
            System.out.println(ex);
        }

        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();
        FXMLAddStudentController toadd = Loader.getController();
        toadd.getid(iid);

    }

    @FXML
    private void listAction(ActionEvent event) throws IOException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLListStu.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
            System.out.println(ex);
        }

        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();

        FXMLListStuController tolist = Loader.getController();
        tolist.setid();
    }

    @FXML
    private void listReceptAction(ActionEvent event) throws IOException {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("FXMLListRecept.fxml"));
        try {
            Loader.load();
        } catch (LoadException ex) {
            System.out.println(ex);
        }

        Parent logparent = Loader.getRoot();
        Scene scene = new Scene(logparent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setTitle("Admin Here!");
        app_stage.setScene(scene);
        app_stage.show();
        FXMLListReceptController torecept = Loader.getController();
        try {
            torecept.getid();
        } catch (JSONException ex) {
            Logger.getLogger(FXMLAdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
